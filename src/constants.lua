--[[
    GLOBAL VARIABLES
    
    JELLY ENGINE
    Maxime Blanc
    https://github.com/salty-max
]]

TITLE = 'JELLY ENGINE'

TILE_SIZE = 16

VIRTUAL_WIDTH = TILE_SIZE * 16
VIRTUAL_HEIGHT = TILE_SIZE * 16
WINDOW_WIDTH = VIRTUAL_WIDTH * 2
WINDOW_HEIGHT = VIRTUAL_HEIGHT * 2

SCALE_FACTOR = 4

WINDOW_CONFIG = {
    vsync = true,
    resizable = true,
    fullscreen = false
}